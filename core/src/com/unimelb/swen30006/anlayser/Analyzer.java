package com.unimelb.swen30006.anlayser;

import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.LinkedList;
import java.util.Queue;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Shape;
import com.unimelb.swen30006.CarAction.AvoidObstacle;
import com.unimelb.swen30006.CarAction.Brake;
import com.unimelb.swen30006.CarAction.ControlVec;
import com.unimelb.swen30006.CarAction.Forward;
import com.unimelb.swen30006.CarAction.Recovery;
import com.unimelb.swen30006.CarAction.TurnLeft;
import com.unimelb.swen30006.CarAction.TurnRight;
import com.unimelb.swen30006.CarAction.Uturn;
import com.unimelb.swen30006.partc.ai.interfaces.PerceptionResponse;
import com.unimelb.swen30006.partc.ai.interfaces.PerceptionResponse.Classification;
import com.unimelb.swen30006.partc.core.objects.Car;
import com.unimelb.swen30006.partc.core.objects.WorldObject;
import com.unimelb.swen30006.partc.roads.Intersection;
import com.unimelb.swen30006.partc.roads.Road;

public class Analyzer {
	private static final float MAX_TURN_VELOCITY = 20f;
	private static final float LASTTIME = 0.45f;
	private Road[] roads;
	private Intersection[] intersections;
	private carStatus cStatus;
	private carDirection cDirection;
	private boolean didAction;
	enum carDirection{north, south, west, east};
	enum carStatus{stop, uturn, forward, avoidObstacle, recovery, recovery2, recovery3, recovery4,lturn, rturn, leftAlittleBit, rightAlittleBit,solveConflict,brake};
	float begin_delta;
	private float lastTime = LASTTIME;
	private double lastX = 0;
	private double lastY = 0;
	private boolean horizontal = true;
	private double stopDistance = 30f + 15f;
	private  double posX;
	private  double posY;
	private Rectangle2D.Double Sightshape;
	private Rectangle2D.Double LeftSightshape;
	private Point2D.Double destination;
	private static int framecount = 0;
	
	Car car;
	Road currentRoad = null;
	
	boolean first = true;
	public Analyzer(Car car,  Intersection[] intersections, Road[] roads){
		this.car = car;
		this.intersections = intersections;
		this.roads = roads;
		cStatus = carStatus.stop;
		cDirection = carDirection.east;
		didAction = false;

	}
	public void update(Queue<Intersection> intersection, PerceptionResponse[] results, int visibility, float delta, Point2D.Double realDestination, boolean hasRoute){
		// Differ car movement horizontal or vertical
		destination = realDestination;
		
		if(!hasRoute){
			// Invalid destination
			return;
		}
		
		if(car.getPosition().distance(realDestination) < 7){
			// Stop the car
			return;
		}
		//280,415
//		System.out.println(car.getPosition());
//		if(cStatus == carStatus.brake && car.getVelocity().len() < 0.05f){
			updateCarDirectionBy2Frames(delta);
//		}else{
//			updateCarDirectionBy2Frames(delta);
//		}
		//treat obstacle
		
		updateStatusFromResponse(results);

			// Perform Turning
		performTurning(intersection);
		
		switch(cStatus){
		case stop:
			if(true){ 
				//判断车头
				cStatus = carStatus.forward;
			}else{
				cStatus = carStatus.uturn;
			}
			break;
		case uturn:
			new Uturn().updateCar(car, delta);
			cStatus = carStatus.forward;
			break;
		case forward:

			new Forward().updateCar(car, delta);
			break;
			
		case rightAlittleBit:
			new AvoidObstacle().updateCar(car, delta);
			cStatus = carStatus.recovery;
			posX = car.getPosition().x;
			posY = car.getPosition().y;
			break;
		case recovery:
//			if((posY - car.getPosition().y ) < 3f){
			if(lastTime > 0){
				lastTime-= delta;
				new ControlVec().updateCar(car, delta);
			}else{
				new Recovery().updateCar(car, delta);
				lastTime= LASTTIME;
				posX = car.getPosition().x;
				posY = car.getPosition().y;
				
				cStatus = carStatus.recovery2;
			}
			break;
			
		case recovery2:
//			if( (car.getPosition().x -posX) < 3f){
			if(lastTime > 0){
				lastTime-= delta;
				new ControlVec().updateCar(car, delta);
			}else{
				cStatus = carStatus.forward;
				lastTime= LASTTIME;
			}
			break;
		case brake:
			cStatus = carStatus.forward;
			new Brake().updateCar(car, delta);
//			new Forward().updateCar(car, delta);
			break;
		
			
		case leftAlittleBit:
//			System.out.println("sss");
			new Recovery().updateCar(car, delta);
			cStatus = carStatus.recovery3;
			break;
		case recovery3:
			if(lastTime > 0){
				lastTime-= delta;
				new ControlVec().updateCar(car, delta);
			
			}else{
				new AvoidObstacle().updateCar(car, delta);
				lastTime= LASTTIME;
				cStatus = carStatus.recovery4;
			}
			break;
			
		case recovery4:
			if(lastTime > 0){
				lastTime-= delta;
				new ControlVec().updateCar(car, delta);
			}else{
				cStatus = carStatus.forward;
				lastTime= LASTTIME;
			}
			break;
			
			
			
		case lturn:
			new TurnLeft().updateCar(car, delta);
			cStatus = carStatus.forward;
			break;
			
		case rturn:
			new TurnRight().updateCar(car, delta);
			cStatus = carStatus.forward;
			break;
			
		default:

		}
	

	}
	private void updateStatusFromResponse(PerceptionResponse[] results) {
//		double Sight_width = ;
//		double Sight_Length =;
//		double LeftSight_width = ;
//		double LeftSight_length = ;
		currentRoad = isOnRoad();
//		System.out.println(cStatus);
		if(currentRoad != null && (cStatus == carStatus.forward||cStatus == carStatus.brake ) ){
//			horizontal = currentRoad.getStartPos().y == currentRoad.getEndPos().y;
			double startPoint_X = currentRoad.getStartPos().x;
			double startPoint_Y = currentRoad.getStartPos().y;
			double endPoint_X = currentRoad.getEndPos().x;
			double endPoint_Y = currentRoad.getEndPos().y;
			float half_width = currentRoad.getWidth() /2;
			float whole_width = currentRoad.getWidth();
			float car_half_width = car.getWidth()/2;
			double car_x = car.getPosition().x;
			double car_y = car.getPosition().y;
//			System.out.println(cDirection);
			switch(cDirection){
				
				case north: //correct
					if(horizontal){ 
						return;
					}
//					System.out.println("nor");
					createSightShape(car.getPosition().x-car.getWidth()/2, car.getPosition().y+10 , car.getWidth(),1.5 * car.getLength());
					createLeftSightShape(car.getPosition().x -15 -car_half_width, car.getPosition().y + 1/2 *car.getLength(),17, 25);
					car.setBoundary(Sightshape);
					if((startPoint_X - car_x) >= 0 && (startPoint_X - car_x) < (half_width)){
						if(hasObjectOnSight(results,Sightshape) !=null){
							cStatus = carStatus.rightAlittleBit;
						}else{
							if(treatTrafficLight(results,"y","big")){
								cStatus = carStatus.brake;
							}else{
								cStatus = carStatus.forward;
							}
						}
					}else if((( car_x- startPoint_X) >= 0 && ( car_x- startPoint_X) < (half_width))){
						if(hasObjectOnSight(results,LeftSightshape) !=null){
//							System.out.println("~~~1");
							if(treatTrafficLight(results,"y","big")){
								
								cStatus = carStatus.brake;
							}else{
								cStatus = carStatus.forward;
							}
						}else{
//							System.out.println("1");
							cStatus = carStatus.leftAlittleBit;
						}
					}
					return;
				case south: 
					if(horizontal){
						return;
					}
					createSightShape(car.getPosition().x-car.getWidth()/2, car.getPosition().y- 1.5*car.getLength(), car.getWidth(), 2 * car.getLength());
					createLeftSightShape(car.getPosition().x ,car.getPosition().y - car.getLength()*2 ,17, 30);
//					car.setBoundary(LeftSightshape);
					if((car_x - startPoint_X) >  0 && (car_x - startPoint_X) < (half_width)){
						if(hasObjectOnSight(results,Sightshape) !=null){
							cStatus = carStatus.rightAlittleBit;
						}else{
							if(treatTrafficLight(results,"y","small")){
								cStatus = carStatus.brake;
							}else{
								cStatus = carStatus.forward;
							}
						}
						
					}else if(( startPoint_X- car_x) >  0 && ( startPoint_X-car_x ) < (half_width)){
						if(hasObjectOnSight(results,LeftSightshape) !=null){
							if(treatTrafficLight(results,"y","small")){
								cStatus = carStatus.brake;
							}else{
								cStatus = carStatus.forward;
							}
						}else{
//							System.out.println("2");
							cStatus = carStatus.leftAlittleBit;
						}
					}
					return;
			
				case east:  //correct
					if(!horizontal){
						return;
					}
//					car.setBoundary(Sightshape);
					createSightShape(car.getPosition().x , car.getPosition().y-car.getWidth()/2, 1.5 * car.getLength(), car.getWidth() * 2);
//					createSightShape(car.getPosition().x, car.getPosition().y+ car.getWidth()/2, 15, 15);
//					createLeftSightShape(car.getPosition().x - 1/2 *car.getLength(), car.getPosition().y + 1/2 * car.getWidth() + currentRoad.getWidth() , 2*car.getLength(), currentRoad.getWidth());
					createLeftSightShape(car.getPosition().x - car.getLength(), car.getPosition().y - 1/2 * car.getWidth() , 17, 20);
					car.setBoundary(Sightshape);
					
					if((car_y - startPoint_Y) >= 0  && (car_y - startPoint_Y) <= (half_width) ){
						Rectangle2D.Double rr = hasObjectOnSight(results,Sightshape) ;
						if(rr != null){

							cStatus = carStatus.rightAlittleBit;
//							System.out.println(rr.toString());
						}else{
							if(treatTrafficLight(results,"x","big")){
								cStatus = carStatus.brake;
							}else{
								cStatus = carStatus.forward;
							}
						}
				
					}else if((startPoint_Y - car_y ) >=0  && (startPoint_Y - car_y  ) <= (half_width)){
							if(hasObjectOnSight(results,LeftSightshape) !=null){
								if(treatTrafficLight(results,"x","big")){
									cStatus = carStatus.brake;
								}else{
									cStatus = carStatus.forward;
								}

							}else{
//								System.out.println("AA" + LeftSightshape.toString());

//								System.out.println("3");
								cStatus = carStatus.leftAlittleBit;
							}
						
					}
					return;
					
				case west:
					if(!horizontal){
						return;
					}
					createSightShape(car.getPosition().x - 1.5*car.getLength(), car.getPosition().y - car.getWidth()/2, 1.5 * car.getLength(), 1.5*car.getWidth());
					createLeftSightShape(car.getPosition().x - 3/2 *car.getLength(), car.getPosition().y -3 * car.getWidth()  ,  17, 20);
					car.setBoundary(LeftSightshape);
					if((startPoint_Y - car_y ) >=0  && (startPoint_Y - car_y  ) <= (half_width) ){
						if(hasObjectOnSight(results,Sightshape) != null){
							cStatus = carStatus.rightAlittleBit;
						}else{
							if(treatTrafficLight(results,"x","small")){
								cStatus = carStatus.brake;
							}else{
								cStatus = carStatus.forward;
							}
						}
						
					}else if((car_y -  startPoint_Y) >=0  && (car_y -  startPoint_Y ) <= (half_width)){
						if(hasObjectOnSight(results,LeftSightshape) != null){
							if(treatTrafficLight(results,"x","small")){
								cStatus = carStatus.brake;
							}else{
								cStatus = carStatus.forward;
							}
						}else{
//							System.out.println("4");
							cStatus = carStatus.leftAlittleBit;
						}
					}
				}
				return;
		
		}else{
			return;
		}
	}
	private boolean treatTrafficLight(PerceptionResponse[] results, String x_y, String big_small) {
		if(currentRoad!=null){
			if(horizontal){
//				System.out.println("east");
				double top = currentRoad.getStartPos().y + currentRoad.getWidth()/2;
				double bottom = currentRoad.getStartPos().y - currentRoad.getWidth()/2;
				if(x_y == "x" && big_small == "big"){
//					System.out.println("hre");
					double tmp = 0f;
					PerceptionResponse res = null;
					for(int i = 0; i < results.length; i++){	
						if(results[i].objectType  ==Classification.TrafficLight){
//							System.out.println(results[i].toString());
//							System.out.println(top);
						}
						if(results[i].objectType  ==Classification.TrafficLight
								&& results[i].position.y == top && results[i].direction.x > 0){
							if((results[i].direction.x * results[i].direction.x+ results[i].direction.y * results[i].direction.y ) > tmp){
								tmp = results[i].direction.x * results[i].direction.x+ 
										results[i].direction.y * results[i].direction.y;
								res = results[i];
							}
						}
						
					}
					if(res!=null &&res.information.get("state") == Color.RED){
//						System.out.println("sss "+res.direction.x );
						
					}
					
					if(res != null  && 
							(res.information.get("state") == Color.RED || res.information.get("state") == Color.YELLOW )
							&&res.direction.x < stopDistance 
							
							){
//						System.out.println("brake");
						return true;
					}else if(res!=null && res.information.get("state") == Color.GREEN && cStatus == carStatus.brake){
						return false;
					}
				}else{
					double tmp = 0f;
					PerceptionResponse res = null;
					for(int i = 0; i < results.length; i++){	
						
						if(results[i].objectType  ==Classification.TrafficLight
								&& results[i].position.y == bottom && results[i].direction.x <= 0){
							if( 
									(results[i].direction.x * results[i].direction.x+ 
									results[i].direction.y * results[i].direction.y )
									> tmp
									
									){
								tmp = results[i].direction.x * results[i].direction.x+ 
										results[i].direction.y * results[i].direction.y;
								res = results[i];
							}
						}
						
					}
					
					if(res != null   && 
							(res.information.get("state") == Color.RED || res.information.get("state") == Color.YELLOW )
							&&(-1)*res.direction.x < stopDistance
							
							){
//						System.out.println("brake");
						return true;
					}else if(res!=null && res.information.get("state") == Color.GREEN && cStatus == carStatus.brake){
						return false;
					}				
				}
			}else{
//				System.out.println("Vertical");
				double top = currentRoad.getStartPos().x - currentRoad.getWidth()/2;
				double bottom = currentRoad.getStartPos().x + currentRoad.getWidth()/2;
				if(x_y == "y" && big_small == "big"){
					double tmp = 0f;
					PerceptionResponse res = null;
					for(int i = 0; i < results.length; i++){	

						if(results[i].objectType ==Classification.TrafficLight
								&& results[i].position.x == top && results[i].direction.y > 0){
							if( 
									(results[i].direction.x * results[i].direction.x+ 
									results[i].direction.y * results[i].direction.y )
									> tmp
									
									){
								tmp = results[i].direction.x * results[i].direction.x+ 
										results[i].direction.y * results[i].direction.y;
								res = results[i];
								
							}
						}
					}
					if(res != null   && 
							(res.information.get("state") == Color.RED || res.information.get("state") == Color.YELLOW )
							&&res.direction.y < stopDistance
							
							){
//						System.out.println("brake");
						return true;
					}else if(res!=null && res.information.get("state") == Color.GREEN && cStatus == carStatus.brake){

						return false;
					}
				}else{
					double tmp = 0f;
					PerceptionResponse res = null;
					for(int i = 0; i < results.length; i++){	
						
						if(results[i].objectType  ==Classification.TrafficLight
								&& results[i].position.x == bottom 
								&& results[i].direction.y < 0){
							if( 
									(results[i].direction.x * results[i].direction.x+ 
									results[i].direction.y * results[i].direction.y )
									> tmp
									){
								tmp = results[i].direction.x * results[i].direction.x+ 
										results[i].direction.y * results[i].direction.y;
								res = results[i];
							}
						}
						
					}
					
					if(res != null  && 
							(res.information.get("state") == Color.RED || res.information.get("state") == Color.YELLOW )
							&&(-1)*res.direction.y < stopDistance
							){
//						System.out.println("brake");
						return true;
					}else if(res!=null && res.information.get("state") == Color.GREEN && cStatus == carStatus.brake){
						return false;
					}				
				}
			}
		}
		return false;
	}
	private Rectangle2D.Double hasObjectOnSight(PerceptionResponse[] results, Rectangle2D.Double Sightshape){
		for(int i = 0; i < results.length; i++){	
			if(results[i].objectType  ==Classification.TrafficLight){
				
			}else {		
				 Rectangle2D.Double rb = new Rectangle2D.Double(results[i].position.x- results[i].length/2,results[i].position.y + results[i].width/2
						,results[i].width, results[i].length);
				if(collidesWith(Sightshape, rb)){
//				if(Sightshape.contains(results[i].position)){
//					System.out.println("collision object:"+results[i].toString());
					return rb;
				}
			}
		}
		return null;
		
	}
	private Intersection isOnIntersection(){
		for(Intersection i: this.intersections){
			if(i.containsPoint(car.getPosition())){
				return i;
			}
		}
		return null;
	}
	
	private void updateCarDirectionBy2Frames(float delta){
		currentRoad = isOnRoad();
		if(currentRoad != null){
		horizontal = currentRoad.getStartPos().y == currentRoad.getEndPos().y;
		}
		double DeltaX = car.getPosition().x - lastX;
		double DeltaY = car.getPosition().y - lastY;
//		if(DeltaX < 0)
//			System.out.println("<0");
		double m = (car.getPosition().y - lastY)/ (car.getPosition().x - lastX);
		if(DeltaX ==  0 && DeltaY >= 0  ){
//			System.out.println("ssss");
			cDirection = carDirection.north;
		}else if(DeltaX == 0 && DeltaY < 0 ){
//			System.out.println(DeltaX+ " " + DeltaY+" "+horizontal);
			cDirection = carDirection.south;
		}else if(DeltaY == 0 && DeltaX < 0){
			cDirection = carDirection.west;
		}else if(DeltaY == 0 && DeltaX >=0){
			cDirection = carDirection.east;
		}else if( m<=1 && m > -1 && DeltaX > 0  ){
			cDirection = carDirection.east;
		}else if(m<1 && m >= -1 && DeltaX < 0  ){
			cDirection = carDirection.west;
		}else if((m>1 || m < -1 )&& DeltaY > 0  ){
//			System.out.println("sssss");
			cDirection = carDirection.north;
		}else{
//			System.out.println(car.getPosition().x +" "+car.getPosition().y +" " +lastX+" "+lastY);
			cDirection = carDirection.south;
		}
		
		lastX = car.getPosition().x;
		lastY = car.getPosition().y;
	}
	
	private void performTurning(Queue<Intersection> intersectionQueue){
		
		// Don't turn until arriving at intersection
		if(isOnIntersection() != null && !didAction){
			///FIXME: For Demo only
			String turningDirection = "";
			turningDirection = determineTurningDirection(intersectionQueue);
			
//			System.out.println("GoTo"+turningDirection);
			// Drive along the dot line
			switch(cDirection){
			case north: //Correct
				if(turningDirection == "north"){
					// Go straight
					didAction = true;
					intersectionQueue.poll();
				}
				else if(turningDirection == "west"){
					// Go left
					if(car.getPosition().y-isOnIntersection().pos.y > intersectionQueue.peek().length/4){
						cStatus = carStatus.lturn;
						didAction = true;
						intersectionQueue.poll();
					}
				}
				else if(turningDirection == "east"){
					// Go right
					if(car.getPosition().y-isOnIntersection().pos.y > intersectionQueue.peek().length/2){
						cStatus = carStatus.rturn;
						didAction = true;
						intersectionQueue.poll();
					}
				}
				else if(turningDirection == "south"){
					///TODO No idea what happen
				}
				break;
			case south: //Correct
				if(turningDirection == "south"){
					// Go straight
					didAction = true;
					intersectionQueue.poll();
				}
				else if(turningDirection == "west"){
					// Go right
					if(car.getPosition().y-isOnIntersection().pos.y < intersectionQueue.peek().length/2){
						cStatus = carStatus.rturn;
						didAction = true;
						intersectionQueue.poll();
					}
				}
				else if(turningDirection == "east"){
					// Go left
					if(car.getPosition().y-isOnIntersection().pos.y < intersectionQueue.peek().length*0.75){
						cStatus = carStatus.lturn;
						didAction = true;
						intersectionQueue.poll();
					}
				}
				else if(turningDirection == "north"){
					///TODO No idea what happen
				}
				break;
			case east: //Correct
				if(turningDirection == "south"){
					// Go right
					if(car.getPosition().x-isOnIntersection().pos.x>intersectionQueue.peek().width/2){
						cStatus = carStatus.rturn;
						didAction = true;
						intersectionQueue.poll();
					}
				}
				else if(turningDirection == "west"){
					// TODO No idea what will happen
				}
				else if(turningDirection == "east"){
					// Go straight
					didAction = true;
					intersectionQueue.poll();
				}
				else if(turningDirection == "north"){
					// Go left
					if(car.getPosition().x-isOnIntersection().pos.x >intersectionQueue.peek().width/4){
						cStatus = carStatus.lturn;
						didAction = true;
						intersectionQueue.poll();
					}
				}
				break;
			case west:// Correct
				if(turningDirection == "south"){
					// Go left
					if(car.getPosition().x-isOnIntersection().pos.x<intersectionQueue.peek().width*0.75){
						cStatus = carStatus.lturn;
						didAction = true;
						intersectionQueue.poll();
					}
				}
				else if(turningDirection == "west"){
					// Go straight
					didAction = true;
					intersectionQueue.poll();
				}
				else if(turningDirection == "east"){
					// TODO No idea what will happen
				}
				else if(turningDirection == "north"){
					// Go right
					if(car.getPosition().x-isOnIntersection().pos.x<intersectionQueue.peek().width/2){
						cStatus = carStatus.rturn;
						didAction = true;
						intersectionQueue.poll();
					}
				}
				break;
			}
		}
		else if(isOnIntersection() == null){
			// Not on Intersection
			didAction = false;
		}
	}

	private String determineTurningDirection(Queue<Intersection> intersectionQueue){
		Queue<Intersection> tempQueue = new LinkedList<Intersection>(intersectionQueue);
		Intersection currentIntersection = tempQueue.peek();
		try{
			currentIntersection = tempQueue.poll();
			Intersection nextIntersection = tempQueue.peek();
			double deltaX = currentIntersection.pos.x - nextIntersection.pos.x;
			double deltaY = currentIntersection.pos.y - nextIntersection.pos.y;
			
			if(deltaX > 0){
				// Go west
				return "west";
			}
			else if(deltaX < 0){
				// Go east
				return "east";
			}
			else if(deltaY > 0){
				// Go south
				return "south";
			}
			else if(deltaY < 0){
				// Go north
				return "north";
			}
			else{
				return "NoAction or UTurn?";
			}
		}catch(NullPointerException exception){
			if(currentIntersection.pos.x+currentIntersection.width/2 == destination.x){
				if(currentIntersection.pos.y+currentIntersection.length/2 > destination.y){
					return "south";
				}
				else{
					return "north";
				}
			}
			if(currentIntersection.pos.y+currentIntersection.width/2 == destination.y){
				if(currentIntersection.pos.x+currentIntersection.length/2 > destination.x){
					return "west";
				}
				else{
					return "east";
				}
			}
			return "No more intersection, prepare to stop";
		}
	}
	private Road isOnRoad(){
		for(Road i: this.roads){
			if(i.containsPoint(car.getPosition())){
				return i;
			}
		}
		return null;
	}
	
	private Road isOnRoad(Point2D.Double position){
		for(Road i: this.roads){
			if(i.containsPoint(position)){
				return i;
			}
		}
		return null;
	}
	private boolean isSameRoad(Road road1, Road road2){
		if(road1 == null || road2 == null){
			return false;
		}
		if(road1.getStartPos() == road2.getStartPos() && road2.getEndPos() == road2.getEndPos()){
			return true;
		}
		return false;
	}
	
	private void createSightShape(double x, double y, double width ,  double height ){
		this.Sightshape = new Rectangle2D.Double(x, y, width, height);
	}
	private void createLeftSightShape(double x, double y,double width ,  double height){
		this.LeftSightshape = new Rectangle2D.Double(x, y, width, height);
	}
	public boolean containsPoint(Rectangle2D.Double rec, Point2D.Double pos){
		return rec.contains(pos);
	}
	public boolean collidesWith(Rectangle2D.Double s, Rectangle2D.Double o){
		return (s.intersects(o) || o.intersects(s));
	}
}

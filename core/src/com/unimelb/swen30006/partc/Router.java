package com.unimelb.swen30006.partc;

import java.awt.geom.Point2D.Double;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Random;
import java.util.Set;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;
import com.badlogic.gdx.utils.XmlReader.Element;
import com.unimelb.swen30006.partc.roads.Intersection;
import com.unimelb.swen30006.partc.roads.Road;

/**
 * 	@author Minxuan Wu, Jie Wen, Yuchieh Tsai
 * 	This map router is intended to calculate the real destination on a particular road within 50 units distance.
 * 	If not, router tell CarPlan that no route can be reached.
 * 	Then router find the shortest path to the real destination, thanks to Dijkstra algorithm.
 * 	Router also calculates the eta for the whole process.
 */
public class Router {

	// Required minimum distance
	private final static float REQ_MIN_DISTANCE = 50;
	// Map Name
	private final static String MAP_FILE = "test_course.xml";

	private Queue<Intersection> ExpectedIntersectionQueue;
	private List<Intersection> IntersectionList = new ArrayList<Intersection>();
	private Double departure;
	private Double destination;

	//Roads and Intersections
	private ArrayList<Road> roads = new ArrayList<Road>();
	private HashMap<String, Intersection> intersections = new HashMap<String, Intersection>();

	// Data structures for loading roads and intersections
	private HashMap<String, Integer> intersectionMap = new HashMap<String, Integer>();
	private HashMap<Intersection, String> reverseMap = new HashMap<Intersection, String>();
	private HashMap<Road, String> roadMap = new HashMap<Road, String>();
	private List<Vertex> vertexes = new ArrayList<Vertex>();
	
	// Used for Dijkstra algorithm
	private Map<String,Float> path = new HashMap<String,Float>();
	private Map<String,String> pathInfo = new HashMap<String,String>();
	private Set<Vertex> open=new HashSet<Vertex>();  
	private Set<Vertex> close=new HashSet<Vertex>();  
	
	public Router(Double departure, Double destination, Queue<Intersection> ExpectedIntersectionQueue) {
		this.destination = destination;
		this.departure = departure;
		this.ExpectedIntersectionQueue = ExpectedIntersectionQueue;
		readMap();
		this.destination = getDestination();
		// destTest();
		
		for(Road r: roadMap.keySet()){
			String start = roadMap.get(r).split("\\|")[0];
			String end = roadMap.get(r).split("\\|")[1];
			int startIndex = intersectionMap.get(start);
			int endIndex = intersectionMap.get(end);
			vertexes.get(startIndex).getChild().put(vertexes.get(endIndex), r.getLength());
			vertexes.get(endIndex).getChild().put(vertexes.get(startIndex), r.getLength());
//			System.out.println(start + "-"+ end +"| "+ startIndex+"-"+endIndex +": "+ r.getLength());
		}
		
		// Fill in the start Vertex
		Vertex start = getDeparture(departure);
		close.add(start);
		for(int i=1;i<vertexes.size();i++){
			if(vertexes.get(i)!=start)
				open.add(vertexes.get(i));
		}
		for(Vertex v: vertexes){
			if(start.getChild().containsKey(v))
				path.put(v.getName(), start.getChild().get(v));
			else{
				path.put(v.getName(), (float) Integer.MAX_VALUE);
			}
			pathInfo.put(v.getName(), start.getName()+"->"+v.getName());
		}
		routeCalculate(vertexes.get(0));
//		printPathInfo();
	}

	// Reuse form the MapReader
	void readMap() {
		try {
			FileHandle file = Gdx.files.internal(MAP_FILE);
			XmlReader reader = new XmlReader();
			Element root = reader.parse(file);
			Element roads = root.getChildByName("roads");
			Array<Element> roadList = roads.getChildrenByName("road");
			// Fill in edge
			for (Element e : roadList) {
				float startX = e.getFloat("start_x");
				float startY = e.getFloat("start_y");
				float endX = e.getFloat("end_x");
				float endY = e.getFloat("end_y");
				float width = e.getFloat("width");
				int numLanes = e.getInt("num_lanes");
				int[] laneMask = {};
				Road road = new Road(new Double(startX, startY), new Double(endX, endY), width, numLanes, laneMask);
				this.roads.add(road);

				// Try to find road with both intersections
				Element i = e.getChildByName("intersections");
				Element start = i.getChildByName("start");
				Element end = i.getChildByName("end");
				if (start != null && end != null) {
					String startID = start.get("id");
					String endID = end.get("id");
					roadMap.put(road, startID + "|" + endID);
				}
			}
			Element intersections = root.getChildByName("intersections");
			Array<Element> intersectionList = intersections.getChildrenByName("intersection");
			// Fill in vertex
			int vertex_count = 0;
			for (Element e : intersectionList) {
				String id = e.get("intersection_id");
				float startX = e.getFloat("start_x");
				float startY = e.getFloat("start_y");
				float width = e.getFloat("width");
				float length = e.getFloat("height");
				Intersection intersection = new Intersection(new Double(startX, startY), width, length);
				this.intersections.put(id, intersection);
				intersectionMap.put(id, vertex_count);
				reverseMap.put(intersection, id);
				vertex_count++;
				vertexes.add(new Vertex(id));
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// Test the real Destination
	public void destTest() {
		for (int i = 0; i < 10; i++) {
			Random r = new Random();
			destination = new Double(r.nextFloat() * 800, r.nextFloat() * 800);
			// destination = new Double(394,236);
			System.out.println("Dest: " + destination);
			System.out.println("Real: " + getDestinationAcc());
			// System.out.println("Real: " + getDestination());
		}
	}

	// Find the first Vertex
	public Vertex getDeparture(Double departure){
		Intersection intersection = findMinDistanceIntersection(departure);
		return vertexes.get(intersectionMap.get(reverseMap.get(intersection)));
	}
	
	// Dijkstra algorithm
	public void routeCalculate(Vertex startV){
		Vertex nearest=getShortestPath(startV);  
        if(nearest==null){  
            return;  
        }  
        close.add(nearest);  
        open.remove(nearest);  
        Map<Vertex,Float> childs=nearest.getChild();  
        for(Vertex child:childs.keySet()){  
            if(open.contains(child)){
            	Float newCompute=path.get(nearest.getName())+childs.get(child);  
                if(path.get(child.getName())>newCompute){
                    path.put(child.getName(), newCompute);
                    startV.getChild().put(child, newCompute); 
                    close.add(startV);
                    pathInfo.put(child.getName(), pathInfo.get(nearest.getName())+"->"+child.getName());  
                }  
            }  
        }  
        routeCalculate(startV);
        routeCalculate(nearest);  
	}
	
	// Find the shortest path for each Vertex
	private Vertex getShortestPath(Vertex node){  
		Vertex result=null;  
        float minDis=Integer.MAX_VALUE;  
        Map<Vertex,Float> childs=node.getChild();  
        for(Vertex child:childs.keySet()){  
            if(open.contains(child)){  
                float distance=childs.get(child);  
                if(distance<minDis){  
                    minDis=distance;  
                    result=child;  
                }  
            }  
        }  
        return result;  
    }  
	
	//Test the shortest route
	public void printPathInfo(){  
        Set<Map.Entry<String, String>> pathInfos=pathInfo.entrySet();  
        for(Map.Entry<String, String> pathInfo:pathInfos){
            System.out.println(pathInfo.getKey()+":"+pathInfo.getValue());  
        }  
    }  
	
	// Fill in the Queue
	public boolean returnRoute(){
		if(setRoute()){
			for(Intersection i: IntersectionList)
				ExpectedIntersectionQueue.add(i);
			return true;
		}else
			return false;
	}
	
	// Post process on the route eliminate the redundant intersections
	private boolean setRoute(){
		if(findMinDistanceRoad(destination)==null){
			return false;
		}
		Intersection end = findMinDistanceIntersection(destination);
		String endID = reverseMap.get(end);
		String route[] = pathInfo.get(endID).split("->");
		System.out.println(pathInfo.get(endID));
		if(route.length==2){
			if(route[0].equals(route[1])){
				IntersectionList.add(intersections.get(route[1]));
			}
			else if(path.get(route[1])==Integer.MAX_VALUE)
				return false;
		}else{
			for(int i=0;i<route.length;i++){
				IntersectionList.add(intersections.get(route[i]));
			}
		}
		for(Road r: roadMap.keySet()){
			String value = roadMap.get(r);
			if(route.length>=2){
				if((route[0]+"|"+route[1]).equals(value)||(route[1]+"|"+route[0]).equals(value)){
					if(r.containsPoint(departure))
						IntersectionList.remove(0);
				}
			}
		}
		int last = route.length-1;
		for(Road r: roadMap.keySet()){
			String value = roadMap.get(r);
			if(route.length>=2){
				if((route[last]+"|"+route[last-1]).equals(value)||(route[last-1]+"|"+route[last]).equals(value)){
					if(r.containsPoint(destination))
						IntersectionList.remove(last);
				}
			}
		}
		return true;
	}
	
	
	// Simplified version: return the middle point of the road(do not know which
	// lane the car should be)
	public Double getDestination() {
		Road road = findMinDistanceRoad(destination);
		System.out.println("Nearest road: " + road);
		if (road == null)
			return null;
//		else if (road.containsPoint(destination))
//			return destination;
		else {
			// Vertical
			if (road.getStartPos().getX() == road.getEndPos().getX()) {
				double start = Math.min(road.getStartPos().getY(), road.getEndPos().getY());
				double end = Math.max(road.getStartPos().getY(), road.getEndPos().getY());
				// Projective point of the destination is between the road
				if (destination.getY() >= start && destination.getY() <= end) {
					return new Double(road.getStartPos().getX(), destination.getY());
				}
				// outside the road
				else if (destination.getY() > end) {
					return new Double(road.getStartPos().getX(), end);
				} else {
					return new Double(road.getStartPos().getX(), start);
				}
			}
			// Horizontal
			else if (road.getStartPos().getY() == road.getEndPos().getY()) {
				double start = Math.min(road.getStartPos().getX(), road.getEndPos().getX());
				double end = Math.max(road.getStartPos().getX(), road.getEndPos().getX());
				// Projective point of the destination is between the road
				if (destination.getX() >= start && destination.getX() <= end) {
					return new Double(destination.getX(), road.getStartPos().getY());
				} else if (destination.getX() > end) {
					return new Double(end, road.getStartPos().getY());
				} else {
					return new Double(start, road.getStartPos().getY());
				}
			} else {
				// Error
				System.out.println("Road error");
				return null;
			}
		}
	}

	// Find the real detination in Double: giving the nearest the point of the
	// destination and the road
	public Double getDestinationAcc() {
		Road road = findMinDistanceRoad(destination);
		System.out.println("Nearest road: " + road);
		if (road == null)
			return null;
		else if (road.containsPoint(destination))
			return destination;
		else {
			// Vertical
			if (road.getStartPos().getX() == road.getEndPos().getX()) {
				double start = Math.min(road.getStartPos().getY(), road.getEndPos().getY());
				double end = Math.max(road.getStartPos().getY(), road.getEndPos().getY());
				// Projective point of the destination is between the road
				if (destination.getY() >= start && destination.getY() <= end) {
					Double realDest = destination;
					if (destination.getX() > road.getStartPos().getX())
						realDest = new Double(road.getStartPos().getX() + road.getWidth() / 2, destination.getY());
					else {
						realDest = new Double(road.getStartPos().getX() - road.getWidth() / 2, destination.getY());
					}
					return realDest;
				}
				// outside the road
				else if (destination.getY() > end) {
					Double realDest = destination;
					if (destination.getX() > road.getStartPos().getX())
						realDest = new Double(road.getStartPos().getX() + road.getWidth() / 2, end);
					else {
						realDest = new Double(road.getStartPos().getX() - road.getWidth() / 2, end);
					}
					return realDest;
				} else {
					Double realDest = destination;
					if (destination.getX() > road.getStartPos().getX())
						realDest = new Double(road.getStartPos().getX() + road.getWidth() / 2, start);
					else {
						realDest = new Double(road.getStartPos().getX() - road.getWidth() / 2, start);
					}
					return realDest;
				}
			}
			// Horizontal
			else if (road.getStartPos().getY() == road.getEndPos().getY()) {
				double start = Math.min(road.getStartPos().getX(), road.getEndPos().getX());
				double end = Math.max(road.getStartPos().getX(), road.getEndPos().getX());
				// Projective point of the destination is between the road
				if (destination.getX() >= start && destination.getX() <= end) {
					Double realDest = destination;
					if (destination.getY() > road.getStartPos().getY())
						realDest = new Double(destination.getX(), road.getStartPos().getY() + road.getWidth() / 2);
					else {
						realDest = new Double(destination.getX(), road.getStartPos().getY() - road.getWidth() / 2);
					}
					return realDest;
				} else if (destination.getX() > end) {
					Double realDest = destination;
					if (destination.getY() > road.getStartPos().getY())
						realDest = new Double(end, road.getStartPos().getY() + road.getWidth() / 2);
					else {
						realDest = new Double(end, road.getStartPos().getY() - road.getWidth() / 2);
					}
					return realDest;
				} else {
					Double realDest = destination;
					if (destination.getY() > road.getStartPos().getY())
						realDest = new Double(end, road.getStartPos().getY() + road.getWidth() / 2);
					else {
						realDest = new Double(end, road.getStartPos().getY() - road.getWidth() / 2);
					}
					return realDest;
				}
			} else {
				// Error
				System.out.println("Road error");
				return null;
			}
		}
	}

	// Calculate the nearest road the destination is
	private Road findMinDistanceRoad(Double position) {
		float minDistance = REQ_MIN_DISTANCE;
		Road objRoad = roads.get(0);
		for (Road r : roads) {
			if (r.minDistanceTo(position) < minDistance) {
				minDistance = r.minDistanceTo(position);
				objRoad = r;
			}
		}
		if (minDistance == 50)
			return null;
		System.out.println("Shortest distance: +" + minDistance);
		return objRoad;
	}

	// Find the nearest intersection
	private Intersection findMinDistanceIntersection(Double position) {
		float minDistance = Integer.MAX_VALUE;
		Intersection objInt = intersections.get(0);
		for (Intersection i : intersections.values()) {
			Double pos = new Double(i.pos.x + i.width/2, i.pos.y+ i.length/2);
			if(pos.distance(position)< minDistance){
				minDistance = (float) pos.distance(position);
				objInt = i;
			}
		}
		System.out.println("Shortest distance: +" + minDistance);
		return objInt;
	}
	
}

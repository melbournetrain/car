package com.unimelb.swen30006.partc.ai.interfaces;

import com.unimelb.swen30006.partc.core.objects.Car;

public interface carAction {
	public void updateCar(Car car, float delta);
}

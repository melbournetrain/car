package com.unimelb.swen30006.partc;

import java.util.HashMap;
import java.util.Map;

public class Vertex {
	private String name;
	private Map<Vertex, Float> child = new HashMap<Vertex, Float>();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Map<Vertex, Float> getChild() {
		return child;
	}

	public void setChild(Map<Vertex, Float> child) {  
        this.child = child;  
    } 
	
	public Vertex(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "[ " + name + " ]";
	}
}

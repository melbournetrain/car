package com.unimelb.swen30006.partc;

import java.awt.geom.Point2D.Double;
import java.util.LinkedList;
import java.util.Queue;

import com.unimelb.swen30006.anlayser.Analyzer;
import com.unimelb.swen30006.partc.ai.interfaces.IPlanning;
import com.unimelb.swen30006.partc.ai.interfaces.PerceptionResponse;
import com.unimelb.swen30006.partc.core.objects.Car;
import com.unimelb.swen30006.partc.roads.Intersection;
import com.unimelb.swen30006.partc.roads.Road;

public class CarPlan implements IPlanning{

	private Car car;
	private Road[] roads;
	private Intersection[] intersections;
	private Router simpleRouter;
	private Double realDestination;
	private Queue<Intersection> ExpectedIntersectionList = new LinkedList<Intersection>();
	private Analyzer simpleAnalyzer;
	private boolean hasRoute;

	public CarPlan(Car car, Road[] roads, Intersection[] intersections){
		this.car = car;
		this.roads = roads;
		this.intersections = intersections;
		// 375, 235 80,140
//		this.hasRoute = planRoute(new Double(375, 235));
		this.hasRoute = planRoute(new Double(280,416));
//		System.out.println(this.hasRoute);
		
		simpleAnalyzer = new Analyzer(car, this.intersections, this.roads);
		
	}
	
	@Override
	public boolean planRoute(Double destination) {
		Double start = car.getPosition();
		simpleRouter = new Router(start, destination, ExpectedIntersectionList);
		realDestination = simpleRouter.getDestination();
//		System.out.println("REAL:"+realDestination);
		return simpleRouter.returnRoute();
	}

	@Override
	public void update(PerceptionResponse[] results, int visibility, float delta) {
		// TODO Auto-generated method stub
		//Anlayzer part
		simpleAnalyzer.update(ExpectedIntersectionList,results,visibility,delta, realDestination, hasRoute);
//		System.out.println("ETA:" + String.format("%.1f", eta()/1000) + "s");
	}

	@Override
	public float eta() {
		// TODO Auto-generated method stub
		
		if(!hasRoute){
			return -1;
		}
		
		Intersection[] array =  ExpectedIntersectionList.toArray(new Intersection[ExpectedIntersectionList.size()]);
		float time = 0;
		for(int i=0;i<array.length;i++){
			if(i==0){
				//From car to first intersection
				double deltaX =  Math.abs(array[i].pos.getX() - car.getPosition().getX());
				double deltaY =  Math.abs(array[i].pos.getY() - car.getPosition().getY());
				if(deltaX > deltaY){
					//Horizontal
					time += deltaX*48;
				}
				else{
					//Vertical
					time += deltaY*48;
				}
				
			}
			else if(i==array.length-1){
				
				break;
			}
			else{
				double deltaX = Math.abs(array[i].pos.getX() - array[i+1].pos.getX());
				double deltaY = Math.abs(array[i].pos.getY() - array[i+1].pos.getY());
				if(deltaX > deltaY){
					//Horizontal
					time += deltaX*48;
				}
				else{
					//Vertical
					time += deltaY*48;
				}
			}
		}
		//From last intersection to destination
		if(array.length != 0){
			double deltaX = Math.abs(array[array.length-1].pos.getX() - realDestination.getX());
			double deltaY = Math.abs(array[array.length-1].pos.getY() - realDestination.getY());
			if(deltaX > deltaY){
				//Horizontal
				time += deltaX*48;
			}
			else{
				//Vertical
				time += deltaY*48;
			}
		}
		else{
			double deltaX =  Math.abs(realDestination.getX() - car.getPosition().getX());
			double deltaY =  Math.abs(realDestination.getY() - car.getPosition().getY());
			if(Math.max(deltaX, deltaY) < 8){
				return 0;
			}
			if(deltaX > deltaY){
				//Horizontal
				time += deltaX*48;
			}
			else{
				//Vertical
				time += deltaY*48;
			}
		}
		return time;
	}

}

package com.unimelb.swen30006.CarAction;

import com.unimelb.swen30006.partc.ai.interfaces.carAction;
import com.unimelb.swen30006.partc.core.objects.Car;

public class AvoidObstacle implements carAction{

	@Override
	public void updateCar(Car car,float delta) {
		// TODO Auto-generated method stub
		if(car.getVelocity().len() >  0.05f){
			car.brake();
		}
		car.turn(-22.5f);
		car.update(delta);
	}

}

package com.unimelb.swen30006.CarAction;

import com.unimelb.swen30006.partc.ai.interfaces.carAction;
import com.unimelb.swen30006.partc.core.objects.Car;

public class Forward implements carAction{

	@Override
	public void updateCar(Car car,float delta) {
		// TODO Auto-generated method stub
		if(car.getVelocity().len() <  21f){
			car.accelerate();
		}else if(car.getVelocity().len() > 20f){
			car.brake();
		}
		car.update(delta);
	}

}

package com.unimelb.swen30006.CarAction;

import com.unimelb.swen30006.partc.ai.interfaces.carAction;
import com.unimelb.swen30006.partc.core.objects.Car;

public class TurnRight implements carAction{

	@Override
	public void updateCar(Car car,float delta) {
		// TODO Auto-generated method stub
		car.turn(-22.5f);
		car.update(delta);
	}

}
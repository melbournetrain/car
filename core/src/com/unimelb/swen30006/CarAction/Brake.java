package com.unimelb.swen30006.CarAction;

import com.unimelb.swen30006.partc.ai.interfaces.carAction;
import com.unimelb.swen30006.partc.core.objects.Car;

public class Brake implements carAction{

	@Override
	public void updateCar(Car car,float delta) {
		// Function of car brake can make the car reverse so we let the velocity never be below a constant.
		if(car.getVelocity().len() > 0.7f){
//			System.out.println(car.getVelocity().len());
			car.brake();
		}
		car.update(delta);
	}

}	
